package cat.itb.wallapopclone.Model;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.ArrayList;

public class Favourite implements Serializable {

    private String key;
    private String emailUsername;
    private String IDproduct;
    private String titulo;
    private String descripcion;
    private String categoria;
    private String subcategoria;
    private String estado;
    private String precio;
    private String moneda;
    private String usuario;
    private ArrayList<String> images;

    public Favourite() {
    }


    public Favourite(String key, String emailUsername, String IDproduct, String titulo, String descripcion, String categoria, String subcategoria, String estado, String precio, String moneda, String usuario, ArrayList<String> images) {
        this.key = key;
        this.emailUsername = emailUsername;
        this.IDproduct = IDproduct;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.categoria = categoria;
        this.subcategoria = subcategoria;
        this.estado = estado;
        this.precio = precio;
        this.moneda = moneda;
        this.usuario = usuario;
        this.images = images;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(String subcategoria) {
        this.subcategoria = subcategoria;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getEmailUsername() {
        return emailUsername;
    }

    public void setEmailUsername(String emailUsername) {
        this.emailUsername = emailUsername;
    }

    public String getIDproduct() {
        return IDproduct;
    }

    public void setIDproduct(String IDproduct) {
        this.IDproduct = IDproduct;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}
