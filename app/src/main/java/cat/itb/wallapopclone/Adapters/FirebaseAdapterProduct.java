package cat.itb.wallapopclone.Adapters;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.like.LikeButton;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import cat.itb.wallapopclone.Activities.ProductActivity;
import cat.itb.wallapopclone.Model.Favourite;
import cat.itb.wallapopclone.Model.Product;
import cat.itb.wallapopclone.R;

public class FirebaseAdapterProduct extends FirebaseRecyclerAdapter<Product, FirebaseAdapterProduct.ProductHolder> {


    DatabaseReference myRef;
    private FirebaseAuth mAuth;
    FirebaseDatabase database;
    FirebaseUser currentUser;
    public static ArrayList<String> imageFavourite = new ArrayList<String>();

    public FirebaseAdapterProduct(@NonNull FirebaseRecyclerOptions<Product> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull FirebaseAdapterProduct.ProductHolder holder, int position, @NonNull Product model) {
        try {
            holder.bind(model);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public FirebaseAdapterProduct.ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("favourites");
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        return new ProductHolder(v);
    }

    public class ProductHolder extends RecyclerView.ViewHolder{

        ImageView imagenProducto;
        TextView precio;
        TextView moneda;
        TextView titulo;
        LikeButton corazon;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            imagenProducto = itemView.findViewById(R.id.imageView_product_item);
            precio = itemView.findViewById(R.id.textView_product_item_precip);
            moneda = itemView.findViewById(R.id.tipo_moneda);
            titulo = itemView.findViewById(R.id.textView_prduct_item_desciption);
            corazon = itemView.findViewById(R.id.star_button);
        }

        public void bind(final Product model) throws IOException {
            //imageView
            if (model.getImages() == null) {
            } else {
                imagenProducto.setImageBitmap(addImage(model.getImages().get(0)));
            }
            precio.setText(model.getPrecio());
            moneda.setText(model.getMoneda());
            titulo.setText(model.getTitulo());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), ProductActivity.class);
                    i.putExtra("model", model);
                    v.getContext().startActivity(i);
                }
            });

            corazon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String key = myRef.push().getKey();
                    Favourite f = null;
                    imageFavourite.add(model.getImages().get(0));
                    f = new Favourite(key,currentUser.getEmail(),model.getKey(),model.getTitulo(),model.getDescripcion(),model.getCategoria(),model.getSubcategoria(),model.getEstado(),model.getPrecio(),model.getMoneda(),currentUser.getDisplayName(), imageFavourite);
                    myRef.child(key).setValue(f);
                    corazon.setEnabled(true);
                    corazon.setLiked(true);
                }
            });

        }
    }
    public void deleteFavourite(int position){
        getSnapshots().getSnapshot(position).getRef().removeValue();
    }

    public Bitmap addImage(String url) throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        URL imageURL = new URL(url);

        URLConnection connection = imageURL.openConnection();
        InputStream iconStream = connection.getInputStream();
        Bitmap bmp = BitmapFactory.decodeStream(iconStream);

        return bmp;
    }
}
