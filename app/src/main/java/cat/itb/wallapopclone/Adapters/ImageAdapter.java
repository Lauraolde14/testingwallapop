package cat.itb.wallapopclone.Adapters;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import cat.itb.wallapopclone.Activities.AddActivity;
import id.zelory.compressor.Compressor;

import static android.app.Activity.RESULT_OK;

public class ImageAdapter extends AppCompatActivity {

    private DatabaseReference myRef;
    private StorageReference storageReference;
    private String imageName;
    private Bitmap thumb_bitmap;
    private byte[] thumb_byte;
    private File url;
    private static final int REQUEST_IMAGE_CAPTURE = 200;
    private ImageView image;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            imageCut(imageUri);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                url = new File(resultUri.getPath());
                Picasso.with(this).load(url).into(image);
            }
        }
    }

    public void imageCut(Uri imageUri) {
        CropImage.activity(imageUri).setGuidelines(CropImageView.Guidelines.ON)
                .setRequestedSize(640, 640)
                .setAspectRatio(2, 2).start( ImageAdapter.this);
    }

    public void imageCompress() {
        try {
            thumb_bitmap = new Compressor(this)
                    .setMaxHeight(125)
                    .setMaxWidth(125)
                    .setQuality(50)
                    .compressToBitmap(url);

        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        thumb_byte = byteArrayOutputStream.toByteArray();

    }
}
