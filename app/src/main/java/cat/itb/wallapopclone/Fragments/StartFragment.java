package cat.itb.wallapopclone.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import cat.itb.wallapopclone.Adapters.FirebaseAdapterProduct;
import cat.itb.wallapopclone.Model.Product;
import cat.itb.wallapopclone.R;

public class StartFragment  extends Fragment {

    RecyclerView recyclerView;


    FirebaseDatabase database;
    DatabaseReference myRef;

    FirebaseAdapterProduct adapterProduct;

    @Override
    public void onStart() {
        super.onStart();
        adapterProduct.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapterProduct.stopListening();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.start_fragment,container,false);

        recyclerView = v.findViewById(R.id.recycler_main);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Product");

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));



        FirebaseRecyclerOptions<Product> options = new FirebaseRecyclerOptions.Builder<Product>()
                .setQuery(myRef, Product.class).build();
        adapterProduct = new FirebaseAdapterProduct(options);
        recyclerView.setAdapter(adapterProduct);

        return v;
    }
}
