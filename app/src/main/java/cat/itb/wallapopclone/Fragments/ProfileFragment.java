package cat.itb.wallapopclone.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import cat.itb.wallapopclone.Activities.MainActivity;
import cat.itb.wallapopclone.Activities.MainScreenActivity;
import cat.itb.wallapopclone.Activities.YourProductsActivity;
import cat.itb.wallapopclone.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {
    private FirebaseAuth mAuth;
    private LinearLayout linerCerrarSession,linearYourProducts;
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInOptions gso;
    private CircleImageView imageView;
    private TextView  user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_fragment,container,false);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        imageView = v.findViewById(R.id.imageView_profile_item);
        user = v.findViewById(R.id.textView_profile_item);
        linearYourProducts  =  v.findViewById(R.id.imageButton_profile_products);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN) .requestIdToken(getString(R.string.default_web_client_id)) .requestEmail() .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);

            String  email = mAuth.getCurrentUser().getEmail();
            String[] emails =email.split("@");
            user.setText(emails[0]);
            if (currentUser.getPhotoUrl()==null){
                Glide.with(this).load(R.drawable.si).into(imageView);
            }else {
                Glide.with(this).load(currentUser.getPhotoUrl()).into(imageView);
            }





        linerCerrarSession = v.findViewById(R.id.linearLayout2);

        linerCerrarSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();

                mGoogleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            Intent  main = new Intent(getContext(), MainActivity.class);
                            startActivity(main);
                        }else {
                            Toast.makeText(getContext(), "No se pudo cerrar sesión con google", Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        });

        linearYourProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i =new Intent(getContext(), YourProductsActivity.class);
                startActivity(i);

            }
        });


        return v;
    }
}
