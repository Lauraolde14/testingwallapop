package cat.itb.wallapopclone.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import cat.itb.wallapopclone.Adapters.ImageAdapter;
import cat.itb.wallapopclone.Model.Product;
import cat.itb.wallapopclone.R;
import id.zelory.compressor.Compressor;

public class AddActivity extends AppCompatActivity {

    MaterialButton detalles;
    TextInputLayout Titulo;
    TextInputEditText titulo;
    TextInputLayout Descripcion;
    TextInputEditText descripcion;
    TextInputLayout Categoria;
    static TextInputEditText categoria;
    TextInputLayout SubCategoria;
    TextInputEditText subcategoria;
    TextInputLayout Estado;
    static TextInputEditText estado;
    TextInputLayout Precio;
    TextInputEditText precio;
    TextInputLayout Moneda;
    AutoCompleteTextView moneda;
    MaterialButton subirProducto;

    FirebaseDatabase database;
    DatabaseReference myRef;
    private FirebaseAuth mAuth;

    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;

    Bundle bundle;

    String t, d, c, s, e, p, m;

    ImageView imageView1;
    ImageView imageView2;
    ImageView imageView3;
    ImageView imageView4;
    ImageView imageView5;
    ImageView imageView6;
    ImageView imageView7;
    ImageView imageView8;
    ImageView imageView9;
    ImageView imageView10;
    ImageView imageView;

    private StorageReference storageReference;
    private String imageName;
    private Bitmap thumb_bitmap;
    private byte[] thumb_byte;
    private File url;
    private static final int REQUEST_IMAGE_CAPTURE = 200;
    public static Uri imageUri;
    public static ArrayList<String> imagenes = new ArrayList<String>();
    public static ArrayList<String> imagenesUpload = new ArrayList<String>();
    public static FirebaseUser currentUser;

    protected void onSaveInstanceState(Bundle save) {
        super.onSaveInstanceState(save);
        t = titulo.getText().toString();
        d = descripcion.getText().toString();
        c = categoria.getText().toString();
        s = subcategoria.getText().toString();
        e = estado.getText().toString();
        p = precio.getText().toString();
        m = moneda.getText().toString();
        save.putString("t", t);
        save.putString("d", d);
        save.putString("c", c);
        save.putString("s", s);
        save.putString("e", e);
        save.putString("p", p);
        save.putString("m", m);
    }

    protected void onRestoreInstanceState(Bundle recover){
        super.onRestoreInstanceState(recover);
        t = recover.getString("t");
        d = recover.getString("d");
        c = recover.getString("c");
        s = recover.getString("s");
        e = recover.getString("e");
        p = recover.getString("p");
        m = recover.getString("m");
        titulo.setText(t);
        descripcion.setText(d);
        categoria.setText(c);
        subcategoria.setText(s);
        estado.setText(e);
        precio.setText(p);
        moneda.setText(m);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product_activity);
        setTheme(R.style.AppTheme);

        storageReference = FirebaseStorage.getInstance().getReference().child("img_compress");
        detalles = findViewById(R.id.button_detalles);
        Titulo = findViewById(R.id.titulo_text_inputLayout);
        titulo = findViewById(R.id.tituloText);
        Descripcion = findViewById(R.id.descripcion_text_inputLayout);
        descripcion = findViewById(R.id.descripcionText);
        Categoria = findViewById(R.id.categoria_text_inputLayout);
        categoria = findViewById(R.id.categoriaText);
        SubCategoria = findViewById(R.id.subcategoria_text_inputLayout);
        subcategoria = findViewById(R.id.subcategoriaText);
        Estado = findViewById(R.id.estado_text_inputLayout);
        estado = findViewById(R.id.estadoText);
        Precio = findViewById(R.id.precio_text_inputLayout);
        precio =  findViewById(R.id.precioText);
        Moneda = findViewById(R.id.moneda_text_inputLayout);
        moneda = findViewById(R.id.autoCompleteMoneda);
        subirProducto = findViewById(R.id.button_subir);
        imageView1 = findViewById(R.id.imageViewFoto1);
        imageView2 = findViewById(R.id.imageViewFoto2);
        imageView3 = findViewById(R.id.imageViewFoto3);
        imageView4 = findViewById(R.id.imageViewFoto4);
        imageView5 = findViewById(R.id.imageViewFoto5);
        imageView6 = findViewById(R.id.imageViewFoto6);
        imageView7 = findViewById(R.id.imageViewFoto7);
        imageView8 = findViewById(R.id.imageViewFoto8);
        imageView9 = findViewById(R.id.imageViewFoto9);
        imageView10 = findViewById(R.id.imageViewFoto10);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Product");
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        categoria.setInputType(InputType.TYPE_NULL);
        estado.setInputType(InputType.TYPE_NULL);

        arrayList = new ArrayList<>();
        arrayList.add("ARS");
        arrayList.add("MXN");
        arrayList.add("COP");
        arrayList.add("€");
        arrayList.add("USD");
        arrayList.add("GBP");
        arrayList.add("BRL");

        arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.spinner_item_moneda, arrayList);
        moneda.setAdapter(arrayAdapter);
        moneda.setThreshold(1);

        detalles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent d = new Intent(AddActivity.this, MainScreenActivity.class);
                startActivity(d);
            }
        });

        estado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent e = new Intent(AddActivity.this, EstadoActivity.class);
                startActivity(e);
            }
        });

        bundle = getIntent().getExtras();
        if(bundle != null) {
            //Categoria
            categoria.setText(bundle.getString("categoria"));
        }

        categoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddActivity.this, CategoriesActivity.class);
                startActivity(i);
            }
        });

        //Firebase

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.imageViewFoto1:
                        CropImage.startPickImageActivity(AddActivity.this);
                        imageView = findViewById(R.id.imageViewFoto1);
                        break;
                    case R.id.imageViewFoto2:
                        CropImage.startPickImageActivity(AddActivity.this);
                        imageView = findViewById(R.id.imageViewFoto2);
                        break;
                    case R.id.imageViewFoto3:
                        CropImage.startPickImageActivity(AddActivity.this);
                        imageView = findViewById(R.id.imageViewFoto3);
                        break;
                    case R.id.imageViewFoto4:
                        CropImage.startPickImageActivity(AddActivity.this);
                        imageView = findViewById(R.id.imageViewFoto4);
                        break;
                    case R.id.imageViewFoto5:
                        CropImage.startPickImageActivity(AddActivity.this);
                        imageView = findViewById(R.id.imageViewFoto5);
                        break;
                    case R.id.imageViewFoto6:
                        CropImage.startPickImageActivity(AddActivity.this);
                        imageView = findViewById(R.id.imageViewFoto6);
                        break;
                    case R.id.imageViewFoto7:
                        CropImage.startPickImageActivity(AddActivity.this);
                        imageView = findViewById(R.id.imageViewFoto7);
                        break;
                    case R.id.imageViewFoto8:
                        CropImage.startPickImageActivity(AddActivity.this);
                        imageView = findViewById(R.id.imageViewFoto8);
                        break;
                    case R.id.imageViewFoto9:
                        CropImage.startPickImageActivity(AddActivity.this);
                        imageView = findViewById(R.id.imageViewFoto9);
                        break;
                    case R.id.imageViewFoto10:
                        CropImage.startPickImageActivity(AddActivity.this);
                        imageView = findViewById(R.id.imageViewFoto10);
                        break;
                    case R.id.button_subir:
                        if(!titulo.getText().toString().isEmpty() && !descripcion.getText().toString().isEmpty() && !estado.getText().toString().isEmpty() && !precio.getText().toString().isEmpty()) {
                            String key = myRef.push().getKey();
                            Product p = new Product(key, titulo.getText().toString(), descripcion.getText().toString(), categoria.getText().toString(), subcategoria.getText().toString(), estado.getText().toString(), precio.getText().toString(), moneda.getText().toString(), currentUser.getEmail(), imagenesUpload);
                            myRef.child(key).setValue(p);
                            Intent i = new Intent(AddActivity.this, MainScreenActivity.class);
                            startActivity(i);
                        }
                        break;
                }
            }
        };

        imageView1.setOnClickListener(listener);
        imageView2.setOnClickListener(listener);
        imageView3.setOnClickListener(listener);
        imageView4.setOnClickListener(listener);
        imageView5.setOnClickListener(listener);
        imageView6.setOnClickListener(listener);
        imageView7.setOnClickListener(listener);
        imageView8.setOnClickListener(listener);
        imageView9.setOnClickListener(listener);
        imageView10.setOnClickListener(listener);
        subirProducto.setOnClickListener(listener);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            imageUri = CropImage.getPickImageResultUri(this, data);
            url = new File(imageUri.getPath());
            imagenes.add(url.toString());
            Picasso.with(this).load(url).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(imageView);
        }
        imageCompress();
    }

    public void imageCompress() {
        try {
            thumb_bitmap = new Compressor(AddActivity.this)
                    .setMaxHeight(125)
                    .setMaxWidth(125)
                    .setQuality(50)
                    .compressToBitmap(url);

        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        thumb_byte = byteArrayOutputStream.toByteArray();
        subirImagenes();
    }

    public void subirImagenes() {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String timestamp = sdf.format(new Date());
            imageName = timestamp + ".jpg";

            final StorageReference ref = storageReference.child(imageName);
            StorageMetadata metadata = new StorageMetadata.Builder()
                    .setCustomMetadata("clave1", "valor1")
                    .setCustomMetadata("clave2", "valor2")
                    .build();

            UploadTask uploadTask = ref.putBytes(thumb_byte, metadata);
            Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw Objects.requireNonNull(task.getException());
                    }
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    Uri downloadUri = task.getResult();
                    imagenesUpload.add(downloadUri.toString());
                }
            });
    }
}
