package cat.itb.wallapopclone.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import cat.itb.wallapopclone.Adapters.SliderAdapter;
import cat.itb.wallapopclone.Model.Product;
import cat.itb.wallapopclone.R;

public class ProductActivity extends AppCompatActivity {
    SliderView sliderView;
    SliderAdapter sliderAdapter;
    MaterialButton chat;
    MaterialToolbar toolbar;

    TextView titulo;
    TextView moneda;
    TextView precio;
    TextView desc;
    TextView estado;
    Bitmap[] bitmaps;

    Bundle bundle;
    Product model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        titulo = findViewById(R.id.textView_titulo);
        moneda = findViewById(R.id.textView_moneda);
        precio = findViewById(R.id.textView_precio);
        desc = findViewById(R.id.textView_desc);
        estado = findViewById(R.id.status);
        sliderView = findViewById(R.id.sliderView);
        toolbar = findViewById(R.id.topAppBar);

        bundle = getIntent().getExtras();
        model = (Product) bundle.get("model");

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent d = new Intent(ProductActivity.this, MainScreenActivity.class);
                startActivity(d);
            }
        });

        try {
            sliderAdapter = new SliderAdapter(addImage(model.getImages()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        sliderView.setSliderAdapter(sliderAdapter);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.DEPTHTRANSFORMATION);

        titulo.setText(model.getTitulo());
        moneda.setText(model.getMoneda());
        desc.setText(model.getDescripcion());
        precio.setText(model.getPrecio());
        estado.setText(model.getEstado());
        model.getUsuario();

        //currentFragment = new MapsFragment();

        //getSupportFragmentManager().beginTransaction().replace(R.id.maps, currentFragment).commit();

        chat = findViewById(R.id.chatButton);

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent e = new Intent(ProductActivity.this, MainScreenActivity.class);
                e.putExtra("buzon", "buzon");
                startActivity(e);
            }
        });
    }

    public Bitmap[] addImage(ArrayList<String> url) throws IOException {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ArrayList<Bitmap> bmp = new ArrayList<Bitmap>();

        for (int i = 0; i < url.size(); i++) {
            URL imageURL = new URL(url.get(i));

            URLConnection connection = imageURL.openConnection();
            InputStream iconStream = connection.getInputStream();
            bmp.add(BitmapFactory.decodeStream(iconStream));
        }

        bitmaps = bmp.toArray(new Bitmap[0]);
        return bitmaps;
    }

}