package cat.itb.wallapopclone.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;
import cat.itb.wallapopclone.R;

public class CategoriesActivity extends AppCompatActivity {

    MaterialButton detalles;
    MaterialButton coches;
    MaterialButton motos;
    MaterialButton motor;
    MaterialButton moda;
    MaterialButton inmobiliaria;
    MaterialButton TV;
    MaterialButton movil;
    MaterialButton informatica;
    MaterialButton deporte;
    MaterialButton bicicletas;
    MaterialButton consolas;
    MaterialButton hogar;
    MaterialButton electrodomesticos;
    MaterialButton cine;
    MaterialButton niños;
    MaterialButton coleccionismo;
    MaterialButton materiales;
    MaterialButton industria;
    MaterialButton empleo;
    MaterialButton servicios;
    MaterialButton otros;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories_activity);
        setTheme(R.style.AppTheme);

        detalles = findViewById(R.id.button_detalles);
        coches = findViewById(R.id.button_coches);
        motos = findViewById(R.id.button_motos);
        motor = findViewById(R.id.button_motor);
        moda = findViewById(R.id.button_moda);
        inmobiliaria = findViewById(R.id.button_inmobiliaria);
        TV = findViewById(R.id.button_TV);
        movil = findViewById(R.id.button_Movil);
        informatica = findViewById(R.id.button_Informatica);
        deporte = findViewById(R.id.button_Deporte);
        bicicletas = findViewById(R.id.button_Bicicletas);
        consolas = findViewById(R.id.button_consolas);
        hogar = findViewById(R.id.button_hogar);
        electrodomesticos = findViewById(R.id.button_electrodomesticos);
        cine = findViewById(R.id.button_cine);
        niños = findViewById(R.id.button_niños);
        coleccionismo = findViewById(R.id.button_coleccionismo);
        materiales = findViewById(R.id.button_materiales);
        industria = findViewById(R.id.button_industria);
        empleo = findViewById(R.id.button_empleo);
        servicios = findViewById(R.id.button_servicios);
        otros = findViewById(R.id.button_otros);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button_detalles:
                        Intent d = new Intent(CategoriesActivity.this, MainScreenActivity.class);
                        startActivity(d);
                        break;
                    case R.id.button_coches:
                        Intent c = new Intent(CategoriesActivity.this, AddActivity.class);
                        String coches = "Coches";
                        c.putExtra("categoria", coches);
                        startActivity(c);
                        break;
                    case R.id.button_motos:
                        Intent m = new Intent(CategoriesActivity.this, AddActivity.class);
                        String motos = "Motos";
                        m.putExtra("categoria", motos);
                        startActivity(m);
                        break;
                    case R.id.button_motor:
                        Intent o = new Intent(CategoriesActivity.this, AddActivity.class);
                        String motor = "Motor y Accesorios";
                        o.putExtra("categoria", motor);
                        startActivity(o);
                        break;
                    case R.id.button_moda:
                        Intent a = new Intent(CategoriesActivity.this, AddActivity.class);
                        String moda = "Moda y Accesorios";
                        a.putExtra("categoria", moda);
                        startActivity(a);
                        break;
                    case R.id.button_inmobiliaria:
                        Intent i = new Intent(CategoriesActivity.this, AddActivity.class);
                        String inmobiliaria = "Inmobiliaria";
                        i.putExtra("categoria", inmobiliaria);
                        startActivity(i);
                        break;
                    case R.id.button_TV:
                        Intent t = new Intent(CategoriesActivity.this, AddActivity.class);
                        String tv = "TV, Audio, Foto";
                        t.putExtra("categoria", tv);
                        startActivity(t);
                        break;
                    case R.id.button_Movil:
                        Intent l = new Intent(CategoriesActivity.this, AddActivity.class);
                        String movil = "Móviles y Telefonía";
                        l.putExtra("categoria", movil);
                        startActivity(l);
                        break;
                    case R.id.button_Informatica:
                        Intent f = new Intent(CategoriesActivity.this, AddActivity.class);
                        String informatica = "Informática y Electrónica";
                        f.putExtra("categoria", informatica);
                        startActivity(f);
                        break;
                    case R.id.button_Deporte:
                        Intent p = new Intent(CategoriesActivity.this, AddActivity.class);
                        String deporte = "Deporte y Ocio";
                        p.putExtra("categoria", deporte);
                        startActivity(p);
                        break;
                    case R.id.button_Bicicletas:
                        Intent b = new Intent(CategoriesActivity.this, AddActivity.class);
                        String bici = "Bicicletas";
                        b.putExtra("categoria", bici);
                        startActivity(b);
                        break;
                    case R.id.button_consolas:
                        Intent s = new Intent(CategoriesActivity.this, AddActivity.class);
                        String consola = "Consolas y Videojuegos";
                        s.putExtra("categoria", consola);
                        startActivity(s);
                        break;
                    case R.id.button_hogar:
                        Intent h = new Intent(CategoriesActivity.this, AddActivity.class);
                        String hogar = "Hogar y Jardín";
                        h.putExtra("categoria", hogar);
                        startActivity(h);
                        break;
                    case R.id.button_electrodomesticos:
                        Intent e = new Intent(CategoriesActivity.this, AddActivity.class);
                        String electro = "Electrodomésticos";
                        e.putExtra("categoria", electro);
                        startActivity(e);
                        break;
                    case R.id.button_cine:
                        Intent n = new Intent(CategoriesActivity.this, AddActivity.class);
                        String cine = "Cine, Libros y Música";
                        n.putExtra("categoria", cine);
                        startActivity(n);
                        break;
                    case R.id.button_niños:
                        Intent w = new Intent(CategoriesActivity.this, AddActivity.class);
                        String niños = "Niños y Bebés";
                        w.putExtra("categoria", niños);
                        startActivity(w);
                        break;
                    case R.id.button_coleccionismo:
                        Intent q = new Intent(CategoriesActivity.this, AddActivity.class);
                        String colec = "Coleccionismo";
                        q.putExtra("categoria", colec);
                        startActivity(q);
                        break;
                    case R.id.button_materiales:
                        Intent z = new Intent(CategoriesActivity.this, AddActivity.class);
                        String mat = "Materiales de construcción";
                        z.putExtra("categoria", mat);
                        startActivity(z);
                        break;
                    case R.id.button_industria:
                        Intent k = new Intent(CategoriesActivity.this, AddActivity.class);
                        String ind = "Industria y Agricultura";
                        k.putExtra("categoria", ind);
                        startActivity(k);
                        break;
                    case R.id.button_empleo:
                        Intent x = new Intent(CategoriesActivity.this, AddActivity.class);
                        String empleo = "Empleo";
                        x.putExtra("categoria", empleo);
                        startActivity(x);
                        break;
                    case R.id.button_servicios:
                        Intent y = new Intent(CategoriesActivity.this, AddActivity.class);
                        String servicio = "Servicios";
                        y.putExtra("categoria", servicio);
                        startActivity(y);
                        break;
                    case R.id.button_otros:
                        Intent j = new Intent(CategoriesActivity.this, AddActivity.class);
                        String otro = "Otros";
                        j.putExtra("categoria", otro);
                        startActivity(j);
                        break;
                }
            }
        };

        detalles.setOnClickListener(listener);
        coches.setOnClickListener(listener);
        motos.setOnClickListener(listener);
        motor.setOnClickListener(listener);
        moda.setOnClickListener(listener);
        inmobiliaria.setOnClickListener(listener);
        TV.setOnClickListener(listener);
        movil.setOnClickListener(listener);
        informatica.setOnClickListener(listener);
        deporte.setOnClickListener(listener);
        bicicletas.setOnClickListener(listener);
        consolas.setOnClickListener(listener);
        hogar.setOnClickListener(listener);
        electrodomesticos.setOnClickListener(listener);
        cine.setOnClickListener(listener);
        niños.setOnClickListener(listener);
        coleccionismo.setOnClickListener(listener);
        materiales.setOnClickListener(listener);
        industria.setOnClickListener(listener);
        empleo.setOnClickListener(listener);
        servicios.setOnClickListener(listener);
        otros.setOnClickListener(listener);
    }
}
