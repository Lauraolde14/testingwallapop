package cat.itb.wallapopclone.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import cat.itb.wallapopclone.Adapters.FirebaseAdapterProduct;
import cat.itb.wallapopclone.Model.Product;
import cat.itb.wallapopclone.R;

public class YourProductsActivity extends AppCompatActivity {


    RecyclerView recyclerView;

    private FirebaseAuth mAuth;
    FirebaseDatabase database;
    DatabaseReference myRef;

    FirebaseAdapterProduct adapterProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_products);


        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        recyclerView = findViewById(R.id.recycler_yourproducts);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Product");
        Query query = myRef.orderByChild("usuario").equalTo(currentUser.getEmail());
        recyclerView.setLayoutManager(new GridLayoutManager(YourProductsActivity.this,2));


        FirebaseRecyclerOptions<Product> options = new FirebaseRecyclerOptions.Builder<Product>()
                .setQuery(query, Product.class).build();
        adapterProduct = new FirebaseAdapterProduct(options);
        recyclerView.setAdapter(adapterProduct);

    }



    @Override
    public void onStart() {
        super.onStart();
        adapterProduct.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapterProduct.stopListening();
    }



}