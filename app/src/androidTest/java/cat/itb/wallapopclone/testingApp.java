package cat.itb.wallapopclone;

import android.view.KeyEvent;

import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.wallapopclone.Activities.AddActivity;
import cat.itb.wallapopclone.Activities.CategoriesActivity;
import cat.itb.wallapopclone.Activities.MainActivity;
import cat.itb.wallapopclone.Activities.MainScreenActivity;
import cat.itb.wallapopclone.Activities.ProductActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.pressKey;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class testingApp {
    @Rule
    public ActivityScenarioRule<MainActivity> mainActivityActivityScenarioRule = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Rule
    public ActivityScenarioRule<MainScreenActivity> activityMainScreenActivityScenarioRule = new ActivityScenarioRule<MainScreenActivity>(MainScreenActivity.class);

    @Rule
    public ActivityScenarioRule<AddActivity> activityAddActivityScenarioRule = new ActivityScenarioRule<AddActivity>(AddActivity.class);

    @Rule
    public ActivityScenarioRule<CategoriesActivity> activityCategoriesActivityScenarioRule = new ActivityScenarioRule<CategoriesActivity>(CategoriesActivity.class);

    @Rule
    public ActivityScenarioRule<ProductActivity> activityProductActivityScenarioRule = new ActivityScenarioRule<ProductActivity>(ProductActivity.class);

    public String EMAIL = "Laura@itb.cat";
    public String PASS = "1234";

    @Test
    public void loginTest() {
        onView(withId(R.id.textLogin2)).perform(click());
        onView(withId(R.id.emailText)).perform(typeText(EMAIL)).perform(pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withId(R.id.contraseñaText)).perform(typeText(PASS)).perform(pressKey(KeyEvent.KEYCODE_ENTER)).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.iniciarButton)).perform(click());
        onView(withId(R.id.startFragment)).check(matches(isDisplayed()));
    }

    public String NAME = "Laura Olmedo";

    @Test
    public void registerTest() {
        onView(withId(R.id.registerButton)).perform(click());
        onView(withId(R.id.nombreText)).perform(typeText(NAME)).perform(pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withId(R.id.emailText)).perform(typeText(EMAIL)).perform(pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withId(R.id.contraseñaText)).perform(typeText(PASS)).perform(pressKey(KeyEvent.KEYCODE_ENTER)).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.iniciarButton)).perform(click());
        onView(withId(R.id.startFragment)).check(matches(isDisplayed()));
    }

    @Test
    public void navigationTest() {
        //Acceder a la aplicación, te lleva al inicio
        loginTest();
        //Acceder al fragment de mensajes
        onView(withId(R.id.messagesFragment)).perform(click());
        //Acceder al fragment del perfil
        onView(withId(R.id.profileFragment)).perform(click());
        //Acceder al fragment de favoritos
        onView(withId(R.id.favouritesFragment)).perform(click());
        //Acceder al formulario de subir producto
        onView(withId(R.id.addFragment)).perform(click());
        //Escoges una categoria para entrar al apartado de la información
        onView(withId(R.id.button_Bicicletas)).perform(click());
        //vuelves al inicio
        onView(withId(R.id.button_detalles)).perform(click());
    }

    @Test
    public void recyclerView() {
        //llamamos al login test (o register) para poder avanzar hasta la pantalla donde se encuentra el recyclerView
        loginTest(); // registerTest();
        onView(withId(R.id.recycler_main)).perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));
        onView(withId(R.id.itemViewProduct)).check(matches(isDisplayed()));
    }

    @Test
    public void useCaseTest() {
        loginTest();
        verProductosRecycler();
        verDetallesProductos();
        crearProducto();
        verMensajes();
        cerrarSession();
    }

    public String TITULO = "Bicicleta";
    public String DESC = "Bicicleta comprada en 2019 de montaña, usada dos veces";
    public String PRECIO = "230";


    public void crearProducto() {
        onView(withId(R.id.addFragment)).perform(click());
        onView(withId(R.id.categoria_activity)).check(matches(isDisplayed()));
        onView(withId(R.id.button_Bicicletas)).perform(click());
        onView(withId(R.id.tituloText)).perform(typeText(TITULO)).perform(pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withId(R.id.descripcionText)).perform(typeText(DESC)).perform(pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withId(R.id.estadoText)).perform(click());
        onView(withId(R.id.estadoText)).perform(click());
        onView(withId(R.id.button_comoNuevo)).perform(click());
        onView(withId(R.id.precioText)).perform(typeText(PRECIO)).perform().perform(pressKey(KeyEvent.KEYCODE_ENTER));
        onView(withId(R.id.button_subir)).perform(click());
    }

    public void verProductosRecycler() {
        onView(withId(R.id.recycler_main)).perform(swipeUp());
    }

    public void verDetallesProductos() {
        onView(withId(R.id.recycler_main)).perform(RecyclerViewActions.actionOnItemAtPosition(1, click()));
        onView(withId(R.id.itemViewProduct)).check(matches(isDisplayed()));
        onView(withId(R.id.itemViewProduct)).perform(ViewActions.pressBack());
        onView(withId(R.id.recycler_main)).perform(RecyclerViewActions.actionOnItemAtPosition(4, click()));
        onView(withId(R.id.itemViewProduct)).check(matches(isDisplayed()));
        onView(withId(R.id.itemViewProduct)).perform(ViewActions.pressBack());
        onView(withId(R.id.recycler_main)).check(matches(isDisplayed()));
    }

    public void verMensajes() {
        onView(withId(R.id.messagesFragment)).perform(click());
        onView(withId(R.id.messagesFragment)).check(matches(isDisplayed()));
    }

    public void cerrarSession() {
        onView(withId(R.id.profileFragment)).perform(click());
        onView(withId(R.id.linearLayout2)).perform(click());
    }
}
